use bevy::prelude::*;
use cell_react::{get_adj_cells, Position};

use crate::{
    BelongsTo, Board, Captured, MetaState, MyComponent, Particle, ParticleColor, ParticlesCount,
    Player, Players, Positions,
};

/// Split and increment handling
#[derive(Event)]
pub struct Increment {
    pub particle: Entity,
    pub position: Position,
    pub quantity: ParticlesCount,
    pub player_color: Srgba,
}

/// Hook to increment the particle
pub fn on_increment(
    trigger: Trigger<Increment>,
    mut players_query: Query<(&Player, &ParticleColor, &mut Captured)>,
    mut particles_query: Query<
        (
            Entity,
            &mut Particle,
            &mut ParticleColor,
            &Position,
            &mut BelongsTo,
        ),
        Without<Player>,
    >,
    state: ResMut<MetaState>,
    board: Res<Board>,
    mut commands: Commands,
) {
    let player_entity = trigger.entity();
    let particle_entity = trigger.event().particle;
    let count = trigger.event().quantity;
    let pos = trigger.event().position;
    let curr_player_color = trigger.event().player_color;
    let (adj_cells, capacity) = get_adj_cells(pos, state.rows, state.cols);

    // Increment the particle if capacity > particle count
    if count.inc() < capacity.into() {
        if let Ok((_, mut particle, mut particle_color, pos, mut belongs_to)) =
            particles_query.get_mut(particle_entity)
        {
            particle.quantity = count.inc();
            particle_color.0 = curr_player_color;
            if let Some(player_captured) = belongs_to.0 {
                if player_captured != player_entity {
                    commands.trigger_targets(
                        Losing {
                            quantity: count as u8,
                            particle_entity,
                        },
                        player_captured,
                    );
                }
            }
            belongs_to.0 = Some(player_entity);
            commands.trigger_targets(
                Capturing {
                    quantity: 1,
                    particle_entity,
                },
                player_entity,
            );
        }
    // Split on this `else condition`
    } else {
        if let Ok((_, mut particle, mut particle_color, pos, mut belongs_to)) =
            particles_query.get_mut(particle_entity)
        {
            particle.quantity = ParticlesCount::Zero;
            particle_color.0 = Srgba::NONE;
            belongs_to.0 = None;
        }
        //split
        for pos in adj_cells {
            let particle_entity = board.matrix[pos.x as usize][pos.y as usize].unwrap();
            if let Ok((_, mut particle, mut particle_color, _, mut belongs_to)) =
                particles_query.get(particle_entity)
            {
                commands.trigger_targets(
                    Increment {
                        particle: particle_entity,
                        position: pos,
                        quantity: particle.quantity,
                        player_color: curr_player_color,
                    },
                    player_entity,
                );
            }
        }
        commands.trigger_targets(
            Losing {
                particle_entity,
                quantity: 1,
            },
            player_entity,
        );
    }
}

// Player capture detection and determination of winner
#[derive(Event)]
pub struct Capturing {
    pub quantity: u8,
    pub particle_entity: Entity,
}

#[derive(Event)]
pub struct Losing {
    pub quantity: u8,
    pub particle_entity: Entity,
}

pub fn on_capturing(
    trigger: Trigger<Capturing>,
    mut players_query: Query<(&Player, &ParticleColor, &mut Captured)>,
) {
    let player_entity = trigger.entity();
    let particle_entity = trigger.event().particle_entity;
    let quantity = trigger.event().quantity;
    if let Ok((mut player, player_color, mut captured)) = players_query.get_mut(player_entity) {
        captured.count += quantity as u32;
    }
}

pub fn on_losing(
    trigger: Trigger<Losing>,
    mut players_query: Query<(&Player, &ParticleColor, &mut Captured)>,
) {
    let player_entity = trigger.entity();
    let particle_entity = trigger.event().particle_entity;
    let quantity = trigger.event().quantity;
    if let Ok((mut player, player_color, mut captured)) = players_query.get_mut(player_entity) {
        captured.count -= quantity as u32;

        if captured.count == 0 {}
    }
}
