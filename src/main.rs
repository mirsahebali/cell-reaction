#![allow(unused)]
#![allow(clippy::too_many_arguments)]

mod animation;
mod components;
mod menu;
mod observers;
mod render_systems;
mod systems;
mod ui_elements;

use bevy::color::palettes::{css as colors, tailwind};
use bevy::{prelude::*, window::WindowResolution};
use bevy_egui::egui::Color32;
use bevy_egui::EguiPlugin;
use cell_react::Position;
use menu::*;
use observers::*;
use render_systems::{draw_grid_system, render_particle_system};
use std::collections::VecDeque;
use systems::{cell_on_click_system, set_cell_on_hover_system};
use ui_elements::*;

pub const DEFAULT_ROWS: u8 = 10;
pub const DEFAULT_COLS: u8 = 10;
pub const DEFAULT_BACKGROUND: Srgba = tailwind::CYAN_900;

#[derive(Debug, Default, PartialEq, Eq, States, Hash, Clone)]
pub enum AppState {
    #[default]
    Menu,
    EditGame,
    InGame,
    Result,
}

#[derive(Debug, Default, PartialEq, Eq, States, Hash, Clone)]
pub enum GameType {
    Quick,
    #[default]
    Custom,
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Hash, SubStates)]
#[source(AppState = AppState::InGame)]
enum InGameState {
    #[default]
    Playing,
    Paused,
}

#[derive(Resource, Debug, Clone)]
pub struct PlayerUIAdding {
    name: String,
    color: [f32; 3],
}

impl PlayerUIAdding {
    pub fn empty() -> Self {
        Self {
            name: String::new(),
            color: [0.0; 3],
        }
    }
    pub fn random() -> Self {
        PlayerUIAdding {
            name: format!("Player-{}", rand::random::<u8>()),
            color: [
                rand::random::<f32>() % 256.0,
                rand::random::<f32>() % 256.0,
                rand::random::<f32>() % 256.0,
            ],
        }
    }
}

#[derive(Resource, Debug, Clone)]
pub struct PlayersUI {
    players: Vec<PlayerUIAdding>,
}

#[derive(Resource, Default)]
pub struct MenuGizmosMoving {
    pub is_moving: bool,
    pub x: f32,
    pub y: f32,
    pub elapsed: f32,
}

#[derive(Resource, Debug, Clone)]
pub struct MenuBackground([u8; 4]);

#[derive(Resource, Debug, Clone)]
pub struct MetaState {
    pub rows: u8,
    pub cols: u8,
    pub curr_player_id: usize,
    pub started: bool,
    pub curr_cell: CellPos,
    pub background_color: Srgba,
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "Cell_Reaction".to_string(),
                resolution: WindowResolution::new(800., 800.).with_scale_factor_override(1.),
                ..Default::default()
            }),
            ..Default::default()
        }))
        .add_plugins(EguiPlugin)
        .add_sub_state::<InGameState>()
        .insert_state(AppState::Menu)
        .insert_state(GameType::Quick)
        .insert_resource(MenuGizmosMoving::default())
        .insert_resource(MenuBackground(DEFAULT_BACKGROUND.to_u8_array()))
        .insert_resource(PlayerUIAdding::random())
        .insert_resource(PlayersUI {
            players: Vec::new(),
        })
        .add_systems(Startup, (setup_camera, spawn_empty_particles).chain())
        .add_systems(Update, update_ui_background)
        .add_systems(
            Update,
            (
                start_menu_ui.run_if(in_state(AppState::Menu)),
                ui_decoration_system.run_if(in_state(AppState::Menu)),
            ),
        )
        .add_systems(
            Update,
            customize_game_ui.run_if(in_state(AppState::EditGame)),
        )
        .add_systems(
            Update,
            (
                ui_player_turn_system,
                draw_grid_system,
                set_cell_on_hover_system,
                render_particle_system,
                cell_on_click_system,
            )
                .run_if(in_state(AppState::InGame))
                .chain(),
        )
        .insert_resource(MetaState::default())
        .insert_resource(Board {
            matrix: vec![vec![None; 10]; 10],
        })
        .insert_resource(ClearColor(Color::Srgba(DEFAULT_BACKGROUND)))
        // change this to spawn as many
        .insert_resource(Players(Vec::new()))
        .insert_resource(PlayersQueue {
            queue: VecDeque::new(),
        })
        .run();
}

#[derive(Resource, Debug, Clone)]
pub struct Board {
    pub matrix: Vec<Vec<Option<Entity>>>,
}

#[derive(Resource, Debug, Clone)]
pub struct Players(pub Vec<Option<Entity>>);

impl Default for MetaState {
    fn default() -> Self {
        Self {
            rows: DEFAULT_ROWS,
            cols: DEFAULT_COLS,
            curr_player_id: 0,
            started: false,
            curr_cell: CellPos { x: 0, y: 0 },
            background_color: DEFAULT_BACKGROUND,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct CellPos {
    pub x: u8,
    pub y: u8,
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ParticlesCount {
    #[default]
    Zero = 0,
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
}
use ParticlesCount::*;

impl ParticlesCount {
    fn inc(&self) -> ParticlesCount {
        match *self {
            Zero => One,
            One => Two,
            Two => Three,
            Three => Four,
            Four => Zero,
        }
    }
}

impl From<u8> for ParticlesCount {
    fn from(val: u8) -> Self {
        match val {
            0 => Zero,
            1 => One,
            2 => Two,
            3 => Three,
            4 => Four,
            _ => Zero,
        }
    }
}

impl From<ParticlesCount> for u8 {
    fn from(val: ParticlesCount) -> Self {
        use ParticlesCount::*;
        match val {
            Zero => 0,
            One => 1,
            Two => 2,
            Three => 3,
            Four => 4,
        }
    }
}
#[derive(Component, Clone, Copy, Debug, Default)]
pub struct Particle {
    pub quantity: ParticlesCount,
}

#[derive(Component, Clone, Copy, Debug, Default, PartialEq)]
pub struct ParticleColor(pub Srgba);

#[derive(Component, Clone, Copy, Debug, Default)]
pub struct BelongsTo(pub Option<Entity>);

pub type MyComponent<'a> = (
    Entity,
    &'a mut Particle,
    &'a mut ParticleColor,
    &'a Position,
    &'a mut BelongsTo,
);

fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

fn spawn_empty_particles(mut commands: Commands, state: Res<MetaState>, mut board: ResMut<Board>) {
    for y in 0..state.rows {
        for x in 0..state.cols {
            let entity = commands
                .spawn((
                    Particle {
                        quantity: ParticlesCount::Zero,
                    },
                    ParticleColor(Srgba::NONE),
                    Position { x, y },
                    BelongsTo(None),
                ))
                .observe(on_increment)
                .observe(on_capturing)
                .observe(on_losing)
                .id();
            board.matrix[x as usize][y as usize] = Some(entity);
        }
    }
}

#[derive(Component, Debug, Clone)]
pub struct Player {
    pub name: String,
    pub started: bool,
    pub eliminated: bool,
}

#[derive(Component, Debug, Clone)]
pub struct Captured {
    particles: Vec<Entity>,
    count: u32,
}

#[derive(Component, Debug, Clone)]
pub struct Positions(pub Vec<Position>);

#[derive(Resource, Debug, Clone)]
pub struct PlayersQueue {
    pub queue: VecDeque<Entity>,
}

impl Player {
    pub fn random() -> Self {
        Self {
            name: format!("Player-{}", rand::random::<i32>()),
            started: false,
            eliminated: false,
        }
    }
}

pub fn spawn_default_players(mut commands: Commands, mut game_players: ResMut<Players>) {
    let players = [
        (
            Player {
                name: "Player 1".to_string(),
                started: false,
                eliminated: false,
            },
            ParticleColor(colors::AQUA),
            Captured {
                particles: Vec::new(),
                count: 0,
            },
        ),
        (
            Player {
                name: "Player 2".to_string(),
                started: false,
                eliminated: false,
            },
            ParticleColor(colors::YELLOW),
            Captured {
                particles: Vec::new(),
                count: 0,
            },
        ),
    ];
    for (index, player) in players.iter().enumerate() {
        let entity = commands
            .spawn(player.clone())
            .observe(on_increment)
            .observe(on_capturing)
            .observe(on_losing)
            .id();
        game_players.0.push(Some(entity));
    }
}

#[cfg(test)]
mod tests;
