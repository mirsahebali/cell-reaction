use crate::{Color, Gizmos};
use bevy::color::Srgba;
use bevy::{color::ColorToComponents, math::vec2};
pub fn draw_fill_circle(
    gizmos: &mut Gizmos,
    center_x: f32,
    center_y: f32,
    default_color: Srgba,
    color: Srgba,
) {
    // Drawing border for the circle particle
    gizmos.circle_2d(vec2(center_x, center_y), 12., Color::Srgba(default_color));
    for i in 0..360 {
        gizmos.line_2d(
            vec2(center_x, center_y),
            vec2(
                center_x + 12. * (i as f32).cos(),
                center_y + 12. * (i as f32).sin(),
            ),
            color,
        )
    }
}
