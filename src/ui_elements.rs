use bevy::prelude::*;
use bevy_egui::{egui, EguiContexts};

use crate::{
    on_capturing, on_increment, on_losing, AppState, Captured, ParticleColor, Player,
    PlayerUIAdding, Players, PlayersQueue, PlayersUI,
};

pub fn customize_game_ui(
    mut commands: Commands,
    mut contexts: EguiContexts,
    mut players: ResMut<PlayersUI>,
    mut game_state: ResMut<NextState<AppState>>,
    mut players_queue: ResMut<PlayersQueue>,
    mut all_players: ResMut<Players>,
) {
    let mut ctx = contexts.ctx_mut();
    ctx.style_mut(|style| {
        style.visuals.popup_shadow = egui::Shadow::NONE;
    });
    egui::Window::new("Customize Game")
        .anchor(egui::Align2::CENTER_CENTER, egui::vec2(0.0, 0.0))
        .show(ctx, |ui| {
            for player in players.players.iter_mut() {
                ui.horizontal(|ui| {
                    ui.color_edit_button_rgb(&mut player.color);
                    ui.text_edit_singleline(&mut player.name);
                });
            }
            if ui.button("Add Players").clicked() {
                players.players.push(PlayerUIAdding::random());
            }
            if ui.button("Start Game").clicked() {
                // Setup and spawn the players created
                for p in parse_player(players.clone()) {
                    let entity = commands
                        .spawn(p)
                        .observe(on_increment)
                        .observe(on_capturing)
                        .observe(on_losing)
                        .id();
                    players_queue.queue.push_back(entity);
                    all_players.0.push(Some(entity));
                }
                game_state.set(AppState::InGame)
            }
        });
}

fn parse_player(players: PlayersUI) -> Vec<(Player, ParticleColor, Captured)> {
    let mut output = Vec::new();
    for player in players.players {
        let new_player = (
            Player {
                name: player.name,
                eliminated: false,
                started: true,
            },
            ParticleColor(Srgba::from_f32_array([
                player.color[0],
                player.color[1],
                player.color[2],
                255.0,
            ])),
            Captured {
                count: 0,
                particles: Vec::new(),
            },
        );
        output.push(new_player);
    }
    output
}
