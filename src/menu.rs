use std::{process::exit, time::Duration};

use bevy::{color::palettes::tailwind, prelude::*};
use bevy_egui::{
    egui::{self, show_tooltip, vec2, Align, Color32, RichText, Stroke},
    EguiContexts,
};

use egui::FontFamily::Proportional;
use egui::FontId;
use egui::TextStyle::*;

use crate::{
    spawn_default_players, AppState, Captured, GameType, InGameState, MenuBackground,
    MenuGizmosMoving, MetaState, ParticleColor, Player, Players,
};

pub fn ui_decoration_system(
    mut contexts: EguiContexts,
    mut commands: Commands,
    mut gizmos: Gizmos,
    mut windows: Query<&Window>,
    mut time: Res<Time>,
    mut moving: ResMut<MenuGizmosMoving>,
    buttons: Res<ButtonInput<MouseButton>>,
    mut game_state: ResMut<MetaState>,
    mut menu_bg: ResMut<MenuBackground>,
) {
    let mut ctx = contexts.ctx_mut();
    ctx.style_mut(|style| style.visuals.popup_shadow = egui::Shadow::NONE);

    let win = windows.get_single();
    let win = win.unwrap();
    let win_size = win.size();
    if let Some(pos) = win.cursor_position() {
        let (x, y) = (pos.x - (win_size.x / 2.0), -(pos.y - (win_size.y / 2.0)));
        gizmos.circle_2d(bevy::math::vec2(x, y), 10., tailwind::YELLOW_300);
        // Make random gizmos on touch originating from here
    };
    egui_extras::install_image_loaders(ctx);

    egui::Window::new("Decoration")
        .collapsible(false)
        .title_bar(false)
        .anchor(egui::Align2::CENTER_TOP, egui::vec2(0.0, 0.0))
        .resizable(false)
        .show(ctx, |ui| {
            ui.color_edit_button_srgba_unmultiplied(&mut menu_bg.0);
            // DEBUG: Fix this
            game_state.background_color = Srgba::from_u8_array(menu_bg.0);
            ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
                ui.heading(
                    RichText::new("Cell Reaction")
                        .color(Color32::LIGHT_BLUE)
                        .size(40.),
                );
                ui.add(
                    egui::Image::new(egui::include_image!("../assets/icon_cell.png"))
                        .max_size(egui::vec2(40.0, 40.0))
                        .bg_fill(Color32::WHITE)
                        .rounding(5.0),
                );
            });
        });
}

pub fn start_menu_ui(
    mut contexts: EguiContexts,
    mut game_state: ResMut<MetaState>,
    mut next_state: ResMut<NextState<AppState>>,
    mut game_type: ResMut<NextState<GameType>>,
    mut commands: Commands,
    mut game_players: ResMut<Players>,
) {
    let mut ctx = contexts.ctx_mut();
    ctx.style_mut(|style| {
        style.visuals.popup_shadow = egui::Shadow::NONE;
    });
    egui::Window::new("Menu")
        .collapsible(false)
        .title_bar(false)
        .anchor(egui::Align2::CENTER_CENTER, egui::vec2(0.0, 0.0))
        .resize(|r| r.default_width(400.).default_height(400.))
        .resizable(false)
        .show(ctx, |ui| {
            ui.set_height(400.);
            if ui
                .add_sized(vec2(200., 100.), egui::Button::new("Quick Start"))
                .clicked()
            {
                game_type.set(GameType::Quick);
                spawn_default_players(commands, game_players);
                next_state.set(AppState::InGame);
            }
            if ui
                .add_sized(vec2(200., 100.), egui::Button::new("Edit Game"))
                .clicked()
            {
                next_state.set(AppState::EditGame);
                game_type.set(GameType::Custom);
            }
            if ui
                .add_sized(vec2(200., 100.), egui::Button::new("Credits"))
                .clicked()
            {
                // Add credits
            }

            if ui
                .add_sized(vec2(200., 100.), egui::Button::new("Quit"))
                .clicked()
            {
                exit(0);
            }
        });
}

pub fn ui_player_turn_system(
    mut contexts: EguiContexts,
    state: Res<MetaState>,
    players: Query<(Entity, &Player, &ParticleColor, &Captured)>,
    mut next_state: ResMut<NextState<AppState>>,
    mut paused_state: ResMut<NextState<InGameState>>,
    mut curr_state: ResMut<State<InGameState>>,
) {
    let players_count = players.iter().len();
    if let Some((entity, curr_player, curr_player_color, captured)) =
        players.iter().collect::<Vec<_>>().get(state.curr_player_id)
    {
        let ctx = contexts.ctx_mut();
        let mut ctx = contexts.ctx_mut();
        ctx.style_mut(|style| {
            style.visuals.popup_shadow = egui::Shadow::NONE;
        });
        egui::Window::new("Turn")
            .fade_in(true)
            .collapsible(false)
            .title_bar(false)
            .anchor(egui::Align2::CENTER_TOP, egui::vec2(0.0, 0.0))
            .resizable(false)
            .show(ctx, |ui| {
                ui.set_max_size(egui::vec2(100., 20.));
                ui.with_layout(egui::Layout::left_to_right(egui::Align::LEFT), |ui| {
                    ui.label(
                        egui::RichText::new(curr_player.name.to_string())
                            .color(egui::Color32::from_rgb(
                                (curr_player_color.0.red * 100.) as u8,
                                (curr_player_color.0.green * 100.) as u8,
                                (curr_player_color.0.blue * 100.) as u8,
                            ))
                            .strong()
                            .size(20.),
                    );
                });
            });
        egui::Window::new("Game Options")
            .fade_in(true)
            .title_bar(false)
            .resizable(false)
            .collapsible(false)
            .anchor(egui::Align2::LEFT_BOTTOM, egui::vec2(0.0, 0.0))
            .show(ctx, |ui| {
                if ui.button("||").clicked() {
                    paused_state.set(match *curr_state.get() {
                        InGameState::Paused => InGameState::Paused,
                        InGameState::Playing => InGameState::Playing,
                    });
                };
            });
    }
}

pub fn update_ui_background(
    mut clear_color: ResMut<ClearColor>,
    mut game_state: ResMut<MetaState>,
) {
    clear_color.0 = Color::Srgba(game_state.background_color);
}
