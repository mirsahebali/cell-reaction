use cell_react::get_adj_cells;

#[test]
fn test_count_neigbours() {
    let (_, capacity) = get_adj_cells(cell_react::Position { x: 2, y: 0 }, 10, 10);
    assert_eq!(capacity, 3);
}
