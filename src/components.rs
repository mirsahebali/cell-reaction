use bevy::prelude::*;

#[derive(Component, Clone, Copy, Debug, Default)]
pub struct Particle {
    pub quantity: u8,
    pub is_filled: bool,
}

#[derive(Component, Clone, Copy, Debug, Default)]
pub struct ParticleColor(pub Srgba);

#[derive(Resource, Debug, Clone)]
pub struct Filled(pub bool);
