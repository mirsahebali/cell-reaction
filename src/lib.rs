pub const CANVAS_SIZE: f32 = 600.;

pub fn get_gap_size(width: f32, height: f32, rows: f32, cols: f32) -> (f32, f32) {
    (width / cols, height / rows)
}

pub fn get_center_pos(
    x: u8,
    y: u8,
    x_gap: f32,
    y_gap: f32,
    margin_x: f32,
    margin_y: f32,
) -> (f32, f32) {
    (
        ((x as f32 * x_gap) + margin_x) + (x_gap / 2.),
        ((y as f32 * y_gap) + margin_y) + (y_gap / 2.),
    )
}

pub fn get_canvas_measurement(physical_width: f32, physical_height: f32) -> (f32, f32) {
    (physical_width * 0.85, physical_height * 0.85)
}

pub fn is_filled(particles: u8, neighbours: u8) -> bool {
    neighbours - 1 == particles
}

use bevy::prelude::*;
#[derive(Component, Default, Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct Position {
    pub x: u8,
    pub y: u8,
}

pub fn get_adj_cells(pos: Position, rows: u8, cols: u8) -> (Vec<Position>, u8) {
    let (x, y) = (pos.x as i8, pos.y as i8);
    let apparent_neighbours = [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)];
    let mut actual_neighbours = Vec::new();
    let mut capacity = 0;
    for pos in apparent_neighbours {
        if pos.0 < 0
            || pos.1 < 0
            || pos.0 >= rows as i8
            || pos.1 >= rows as i8
            || pos.0 >= cols as i8
            || pos.1 >= cols as i8
        {
            continue;
        } else {
            capacity += 1;
            actual_neighbours.push(Position {
                x: pos.0 as u8,
                y: pos.1 as u8,
            });
        }
    }
    (actual_neighbours, capacity)
}

pub fn get_adjacent_cells(pos: Position, rows: u8, cols: u8) -> (Vec<Position>, u8) {
    let (x, y) = (pos.x as i8, pos.y as i8);
    let apparent_neighbours = [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)];
    let mut actual_neighbours = Vec::new();
    let mut capacity = 0;
    for pos in apparent_neighbours {
        if pos.0 < 0
            || pos.1 < 0
            || pos.0 >= rows as i8
            || pos.1 >= rows as i8
            || pos.0 >= cols as i8
            || pos.1 >= cols as i8
        {
            continue;
        } else {
            capacity += 1;
            actual_neighbours.push(Position {
                x: pos.0 as u8,
                y: pos.1 as u8,
            });
        }
    }
    (actual_neighbours, capacity)
}
