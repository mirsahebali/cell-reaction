use crate::{
    animation::draw_fill_circle, Board, MetaState, MyComponent, ParticleColor, ParticlesCount,
    Player, Players,
};
use bevy::{
    math::{uvec2, vec2},
    prelude::*,
};

use cell_react::{get_canvas_measurement, get_center_pos, get_gap_size, Position};

pub fn draw_grid_system(
    mut gizmos: Gizmos,
    game_state: Res<MetaState>,
    window_query: Query<&Window>,
    players: Query<(&Player, &ParticleColor)>,
    all_players: Res<Players>,
) {
    let players_count = all_players.0.len();
    if let Some((curr_player, curr_player_color)) = players
        .iter()
        .collect::<Vec<_>>()
        .get(game_state.curr_player_id % players_count)
    {
        if let Ok(win) = window_query.get_single() {
            let (width, height) = get_canvas_measurement(win.width(), win.height());
            let (x_gap, y_gap) = get_gap_size(
                width,
                height,
                game_state.rows as f32,
                game_state.cols as f32,
            );
            gizmos.rect_2d(
                vec2(0.0, 0.0),
                0.,
                Vec2::from_array([width, height]),
                Color::WHITE,
            );
            gizmos.grid_2d(
                vec2(0., 0.),
                0.,
                uvec2(game_state.rows.into(), game_state.cols.into()),
                vec2(x_gap, y_gap),
                curr_player_color.0,
            );
        }
    };
}

pub fn render_particle_system(
    particle_query: Query<MyComponent>,
    board: Res<Board>,
    mut gizmos: Gizmos,
    window_query: Query<&Window>,
    state: Res<MetaState>,
    time: Res<Time>,
) {
    if let Ok(win) = window_query.get_single() {
        let (width, height) = get_canvas_measurement(win.width(), win.height());
        let (x_gap, y_gap) = get_gap_size(width, height, state.cols as f32, state.rows as f32);
        let (margin_x, margin_y) = ((win.width() - width) / 2., (win.height() - height) / 2.);

        for i in 0..state.rows {
            for j in 0..state.cols {
                let entity = board.matrix[i as usize][j as usize].unwrap();
                if let Ok((ent, particle, particle_color, pos, _)) = particle_query.get(entity) {
                    let (center_x, center_y) =
                        get_center_pos(i, j, x_gap, y_gap, margin_x, margin_y);

                    // Adjusting from centered of the origin of circle(0,0) relative to mouse position for gizmos
                    let (oc_x, oc_y) = (
                        (center_x - (win.width() / 2.)),
                        -(center_y - (win.height() / 2.)),
                    );
                    let time_elapsed = time.elapsed_seconds();

                    let (center_vx, center_vy) = (oc_x, oc_y + (time_elapsed.sin() * 10.));
                    // [`center_vx`, `center_vy`] is x and y coordinate variable center for *horizontal* up
                    // and down motion of the particle in positive direction
                    let (center_hx, center_hy) = (oc_x + (time_elapsed.cos() * 10.), oc_y);

                    // [`center_vx_neg`, `center_vy_neg`] is x and y coordinate variable center for *horizontal* up
                    // and down motion of the particle in negative direction
                    let (center_hx_neg, center_hy_neg) = (oc_x - (time_elapsed.cos() * 10.), oc_y);
                    let (_center_vx_neg, _center_vy_neg) =
                        (oc_x, oc_y - (time_elapsed.sin() * 10.));
                    use ParticlesCount::*;
                    match particle.quantity {
                        One => {
                            draw_fill_circle(
                                &mut gizmos,
                                center_vx,
                                center_vy,
                                state.background_color,
                                particle_color.0,
                            );
                        }
                        Two => {
                            draw_fill_circle(
                                &mut gizmos,
                                center_vx,
                                center_vy,
                                state.background_color,
                                particle_color.0,
                            );
                            draw_fill_circle(
                                &mut gizmos,
                                center_hx,
                                center_hy,
                                state.background_color,
                                particle_color.0,
                            );
                        }
                        Three => {
                            draw_fill_circle(
                                &mut gizmos,
                                center_vx,
                                center_vy,
                                state.background_color,
                                particle_color.0,
                            );
                            draw_fill_circle(
                                &mut gizmos,
                                center_hx,
                                center_hy,
                                state.background_color,
                                particle_color.0,
                            );
                            draw_fill_circle(
                                &mut gizmos,
                                center_hx_neg,
                                center_hy_neg,
                                state.background_color,
                                particle_color.0,
                            );
                        }
                        _ => {
                            // draw_fill_circle(
                            //     &mut gizmos,
                            //     center_vx,
                            //     center_vy,
                            //     state.background_color,
                            //     color.0,
                            // );
                        }
                    }
                }
            }
        }
    }
}
