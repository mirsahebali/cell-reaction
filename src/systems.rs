use crate::{Board, Captured, Increment, MyComponent, Players};
use crate::{CellPos, MetaState, ParticleColor, Player};
use bevy::prelude::*;
use cell_react::{get_canvas_measurement, get_gap_size};

pub fn set_cell_on_hover_system(window_query: Query<&Window>, mut game_state: ResMut<MetaState>) {
    if let Ok(win) = window_query.get_single() {
        let (width, height) = get_canvas_measurement(win.width(), win.height());

        let (x_gap, y_gap) = get_gap_size(
            width,
            height,
            game_state.cols as f32,
            game_state.rows as f32,
        );

        let (margin_x, margin_y) = ((win.width() - width) / 2., (win.height() - height) / 2.);

        let (mut x, mut y) = (0u8, 0u8);
        if let Some(pos) = win.cursor_position() {
            x = ((pos.x - margin_x) / x_gap) as u8;
            y = ((pos.y - margin_y) / y_gap) as u8;
        }
        game_state.curr_cell = CellPos { x, y };
    }
}

pub fn cell_on_click_system(
    mut state: ResMut<MetaState>,
    all_players: Res<Players>,
    buttons: Res<ButtonInput<MouseButton>>,
    mut players_query: Query<(Entity, &mut Player, &ParticleColor, &mut Captured)>,
    mut particles_query: Query<MyComponent, Without<Player>>,
    board: Res<Board>,
    mut commands: Commands,
) {
    let CellPos { x, y } = state.curr_cell;
    if buttons.just_pressed(MouseButton::Left) && (x < 10 && y < 10) {
        state.started = true;
        // Increment the current player id
        let entity = board.matrix[x as usize][y as usize].unwrap();
        let (entity, particle, pariticle_color, pos, _) = particles_query.get_mut(entity).unwrap();
        if let Some(player_entity) = all_players.0[state.curr_player_id] {
            if let Ok((player_entity, mut curr_player, curr_player_color, _)) =
                players_query.get_mut(player_entity)
            {
                curr_player.started = true;

                // Do not increment particle if color of current player doesn't match color of the
                // particle in the current cell
                if pariticle_color.0 != Srgba::NONE && curr_player_color.0 != pariticle_color.0 {
                    return;
                }
                state.curr_player_id = (state.curr_player_id + 1) % all_players.0.len();
                commands.trigger_targets(
                    Increment {
                        particle: entity,
                        position: *pos,
                        quantity: particle.quantity,
                        player_color: curr_player_color.0,
                    },
                    player_entity,
                );
            };
        }
    }
}
